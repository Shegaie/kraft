// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Kraft/Player/KraftPlayerState.h"

#include "Leader.generated.h"

#define LEADER_ATTACK_COOLDOWN 2

class KRAFT_API AWorker;

class KRAFT_API AKraftUnit;

UCLASS()
class KRAFT_API ALeader : public ACharacter
{
	GENERATED_BODY()

public:
	ALeader();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	UFUNCTION()
    void InitUnit(AKraftPlayerState *PlayerStateInit);

	UFUNCTION(BlueprintCallable)
    void CheckActorInAttackZone(TArray<AActor*> OtherArray, const float DeltaTime);

	UFUNCTION()
    virtual void Select(bool IsSelectedNewValue);

	UFUNCTION()
    void MoveToLocation(FVector Location);

	UFUNCTION(Server, Unreliable)
	void AttackUnit(AKraftUnit* Target, int _Damage);
	
	UFUNCTION(Server, Unreliable)
    void AttackWorker(AWorker* Target, int _Damage);
	
	UFUNCTION(Server, Unreliable)
    void AttackLeader(ALeader* Target, int _Damage);
	
	UFUNCTION()
    void RemoveLife(int _Damage);

	UPROPERTY(EditAnywhere, BlueprintReadOnly, ReplicatedUsing=UpdateMaterialColor)
	FLinearColor Color;

	UPROPERTY()
	float CoolDown = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool IsAttacking = false;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float HealthPoints = 500;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float Strength = 100;

	UFUNCTION()
	void SetMaxSpeed(float NewSpeed);
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MaxSpeed = 300;

	// Returns CursorToWorld
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// A decal that projects to the cursor location.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;

	UPROPERTY()
	AKraftPlayerState *PlayerStatePtr;

	UFUNCTION()
    void UpdateMaterialColor();
};
