// Fill out your copyright notice in the Description page of Project Settings.

#include "Worker.h"

#include "GeneratedCodeHelpers.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Components/CapsuleComponent.h"
#include "Components/DecalComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kraft/Player/KraftPlayerController.h"

// Sets default values
AWorker::AWorker()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(25.2f, 57.6f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Configure character max. speed
	GetCharacterMovement()->MaxWalkSpeed = 450;

	// Set this character to call Tick() every frame. You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(
		TEXT("Material'/Game/Kraft_BP/Blueprints/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded()) {
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());
	CursorToWorld->SetVisibility(false);
}

void AWorker::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AWorker, Color);
}

// Called when the game starts or when spawned
void AWorker::BeginPlay()
{
	Super::BeginPlay();
}

// Called to bind functionality to input
void AWorker::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AWorker::InitUnit(AKraftPlayerState* PlayerStateInit)
{
	if (HasAuthority() && GetOwner()) {
		PlayerStatePtr = PlayerStateInit;
		FString PlayerName = PlayerStatePtr->GetPlayerName();
		Color = PlayerStatePtr->GetColor();
		if (HasAuthority()) {
			UpdateMaterialColor();
		}
	}
}

void AWorker::UpdateMaterialColor()
{
	TArray<class UMaterialInterface*> Materials = GetMesh()->GetMaterials();
	for (int i = 0; i < Materials.Num(); i++) {
		UMaterialInterface* MaterialInterface = GetMesh()->GetMaterial(i);
		UMaterialInstanceDynamic* DynamicMaterial = GetMesh()->CreateDynamicMaterialInstance(i, MaterialInterface);
		if (DynamicMaterial != nullptr) {
			DynamicMaterial->SetVectorParameterValue("BodyColor", Color);
		}
	}
}

void AWorker::HandleRessourceState()
{
	AKraftPlayerController* PlayerControllerPtr = Cast<AKraftPlayerController>(GetOwner());
	if (WorkerState == EWorkerState::MovingToRessourcePit) {
		if ((GetActorLocation() - ActiveRessourcePit->GetActorLocation()).Size() < 150) {
			SetWorkerState(EWorkerState::MovingToBase);
			if (NearestBase) {
				PlayerControllerPtr->MoveWorker(this, NearestBase->GetActorLocation());
			}
		}
	} else if (WorkerState == EWorkerState::MovingToBase) {
		if ((GetActorLocation() - NearestBase->GetActorLocation()).Size() < 250) {
			SetWorkerState(EWorkerState::MovingToRessourcePit);
			AKraftPlayerState *PlayerStatePtrLocal = Cast<AKraftPlayerController>(GetOwner())->GetPlayerState<AKraftPlayerState>();
			if (PlayerStatePtrLocal) {
				PlayerStatePtrLocal->AddMineral(RESSOURCE_PIT_MINERAL_AMOUNT);
				PlayerControllerPtr->MoveWorker(this, ActiveRessourcePit->GetActorLocation());
			}
		}
	}
}

void AWorker::MoveOnNetwork_Implementation(AWorker* Worker, FVector Location)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::White, FString::Printf(TEXT("sex")));
	if (!HasAuthority())
		return;

	Worker->MoveToLocation(Location);
}

void AWorker::SetWorkerState(const EWorkerState State)
{
	WorkerState = State;
}

void AWorker::SetWorkerTask(const EWorkerTask Task)
{
	WorkerTask = Task;
}

// Called every frame
void AWorker::Tick(const float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (CursorToWorld != nullptr) {
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled()) {
			if (GetWorld()) {
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				Params.AddIgnoredActor(this);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		} else if (APlayerController* PC = Cast<APlayerController>(GetController())) {
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}
	HandleRessourceState();
}

void AWorker::Select(const bool IsSelectedNewValue)
{
	CursorToWorld->SetVisibility(IsSelectedNewValue);
}

void AWorker::MoveToLocation(const FVector Location)
{
	if (HasAuthority()) {
		UAIBlueprintHelperLibrary::SimpleMoveToLocation(GetController(), Location);
	}
}

void AWorker::SetActiveRessourcePit(ARessourcePit* RessourcePit)
{
	ActiveRessourcePit = RessourcePit;
}

void AWorker::SetActiveBase(ABase* Base)
{
	NearestBase = Base;
}

bool AWorker::HasAssignedRessourcePit()
{
	return !(ActiveRessourcePit == nullptr);
}
