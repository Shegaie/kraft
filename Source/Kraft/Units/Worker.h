// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "KraftUnit.h"
#include "Kraft/Buildings/Base.h"
#include "Kraft/Level/RessourcePit.h"

#include "Worker.generated.h"

#define RESSOURCE_PIT_MINERAL_AMOUNT 5

UENUM( BlueprintType )
enum class EWorkerState: uint8 {
	Rest,
    MovingToLocation,
    MovingToRessourcePit,
    MovingToBase,
    MovingToBuilding
};

UENUM ( BlueprintType )
enum class EWorkerTask: uint8 {
	None,
    ToLocation,
    MiningRessource,
    Building
};

class AKraftPlayerController;

UCLASS()
class KRAFT_API AWorker : public ACharacter
{
	GENERATED_BODY()

public:
	AWorker();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	UFUNCTION()
	void InitUnit(AKraftPlayerState *PlayerStateInit);

	UFUNCTION()
    virtual void Select(bool IsSelectedNewValue);

	UFUNCTION()
    void MoveToLocation(FVector Location);

	UFUNCTION()
	void SetActiveRessourcePit(ARessourcePit *RessourcePit);

	UFUNCTION()
	void SetActiveBase(ABase *Base);

	UFUNCTION()
	void SetWorkerState(EWorkerState State);

	UFUNCTION()
	void SetWorkerTask(EWorkerTask Task);

	UFUNCTION()
	bool HasAssignedRessourcePit();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, ReplicatedUsing=UpdateMaterialColor)
	FLinearColor Color;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float HealthPoints = 100;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float Strength = 10;

	// Returns CursorToWorld
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// A decal that projects to the cursor location.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;

	UPROPERTY()
	AKraftPlayerState *PlayerStatePtr;
	
	UFUNCTION()
    void UpdateMaterialColor();

	UFUNCTION()
	void HandleRessourceState();

	UFUNCTION(Server, Unreliable)
	void MoveOnNetwork(AWorker *Worker, FVector Location);

	UPROPERTY()
	ARessourcePit *ActiveRessourcePit;

	UPROPERTY()
	ABase *NearestBase;

	UPROPERTY()
	EWorkerState WorkerState = EWorkerState::Rest;

	UPROPERTY()
	EWorkerTask WorkerTask = EWorkerTask::None;
};
