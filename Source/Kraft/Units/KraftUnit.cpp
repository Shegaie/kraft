// Fill out your copyright notice in the Description page of Project Settings.

#include "KraftUnit.h"
#include "GameFramework/Actor.h"

#include "GeneratedCodeHelpers.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Components/CapsuleComponent.h"
#include "Components/DecalComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kraft/Player/KraftPlayerController.h"
#include "Worker.h"
#include "Leader.h"

AKraftUnit::AKraftUnit()
{
    // Set size for player capsule
    GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

    // Don't rotate character to camera direction
    bUseControllerRotationPitch = false;
    bUseControllerRotationYaw = false;
    bUseControllerRotationRoll = false;

    // Configure character movement
    GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
    GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
    GetCharacterMovement()->bConstrainToPlane = true;
    GetCharacterMovement()->bSnapToPlaneAtStart = true;

    // Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
    bReplicates = true;

    // Create a decal in the world to show the cursor's location
    CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
    CursorToWorld->SetupAttachment(RootComponent);
    static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Kraft_BP/Blueprints/M_Cursor_Decal.M_Cursor_Decal'"));
    if (DecalMaterialAsset.Succeeded()) {
        CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
    }
    CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
    CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());
    CursorToWorld->SetVisibility(false);
}

void AKraftUnit::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);
    DOREPLIFETIME(AKraftUnit, Color);
    DOREPLIFETIME(AKraftUnit, Health);
    DOREPLIFETIME(AKraftUnit, Damage);
}

void AKraftUnit::InitUnit(AKraftPlayerState* PlayerStateInit)
{
    if (HasAuthority() && GetOwner()) {
        PlayerStatePtr = PlayerStateInit;
        FString PlayerName = PlayerStatePtr->GetPlayerName();
        Color = PlayerStatePtr->GetColor();
        Health = PlayerStatePtr->GetUnitsHealth();
        Damage = PlayerStatePtr->GetUnitsDamage();
        if (HasAuthority()) {
            UpdateMaterialColor();
        }
    }
}

void AKraftUnit::CheckActorInAttackZone(TArray<AActor*> OtherArray, const float DeltaTime)
{
    CoolDown += DeltaTime;
    bool HasAttacked = false;

    for (AActor *Actor: OtherArray) {
        ALeader *LeaderEnnemy = Cast<ALeader>(Actor);
        if (LeaderEnnemy && !Color.Equals(LeaderEnnemy->Color)) {
            IsAttacking = true;
            HasAttacked = true;
            if (CoolDown > ATTACK_COOLDOWN) {
                AttackLeader(LeaderEnnemy, Damage);
                CoolDown = 0;
                break;
            }
        }
        AKraftUnit *UnitEnnemy = Cast<AKraftUnit>(Actor);
        if (UnitEnnemy && !Color.Equals(UnitEnnemy->Color)) {
            IsAttacking = true;
            HasAttacked = true;
            if (CoolDown > ATTACK_COOLDOWN) {
                AttackUnit(UnitEnnemy, Damage);
                CoolDown = 0;
                break;
            }
        }
        AWorker *WorkerEnnemy = Cast<AWorker>(Actor);
        if (WorkerEnnemy && !Color.Equals(WorkerEnnemy->Color)) {
            IsAttacking = true;
            HasAttacked = true;
            if (CoolDown > ATTACK_COOLDOWN) {
                AttackWorker(WorkerEnnemy, Damage);
                CoolDown = 0;
                break;
            }
        }
    }
    if (!HasAttacked)
        IsAttacking = false;
}

// Called when the game starts or when spawned
void AKraftUnit::BeginPlay()
{
    Super::BeginPlay();
}

void AKraftUnit::UpdateMaterialColor()
{
    TArray<class UMaterialInterface*> Materials = GetMesh()->GetMaterials();
    for (int i = 0; i < Materials.Num(); i++) {
        UMaterialInterface* MaterialInterface = GetMesh()->GetMaterial(i);
        UMaterialInstanceDynamic* DynamicMaterial = GetMesh()->CreateDynamicMaterialInstance(i, MaterialInterface);
        if (DynamicMaterial != nullptr)
            DynamicMaterial->SetVectorParameterValue("BodyColor", Color);
    }
}

void AKraftUnit::RepHealth()
{
}

void AKraftUnit::RepDamage()
{
}

void AKraftUnit::Tick(const float DeltaTime)
{
    Super::Tick(DeltaTime);

    if (CursorToWorld != nullptr) {
        if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled()) {
            if (GetWorld()) {
                FHitResult HitResult;
                FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
                Params.AddIgnoredActor(this);
                FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
                CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
            }
        } else if (APlayerController* PC = Cast<APlayerController>(GetController())) {
            FHitResult TraceHitResult;
            PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
            FVector CursorFV = TraceHitResult.ImpactNormal;
            FRotator CursorR = CursorFV.Rotation();
            CursorToWorld->SetWorldLocation(TraceHitResult.Location);
            CursorToWorld->SetWorldRotation(CursorR);
        }
    }
}

void AKraftUnit::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AKraftUnit::Select(const bool IsSelectedNewValue)
{
    CursorToWorld->SetVisibility(IsSelectedNewValue);
}

void AKraftUnit::MoveToLocation(FVector Location)
{
    if (HasAuthority())
        UAIBlueprintHelperLibrary::SimpleMoveToLocation(GetController(), Location);
}

void AKraftUnit::AttackUnit_Implementation(AKraftUnit* Target, const int _Damage)
{
    Target->RemoveLife(_Damage);
    if (Target->Health < 0) {
        Target->Destroy();
    }
}

void AKraftUnit::AttackWorker_Implementation(AWorker* Target, int _Damage)
{
    Target->HealthPoints -= _Damage;
    if (Target->HealthPoints < 0) {
        Target->Destroy();
    }
}

void AKraftUnit::AttackLeader_Implementation(ALeader* Target, int _Damage)
{
    Target->HealthPoints -= _Damage;
    if (Target->HealthPoints < 0) {
        Target->Destroy();
    }
}

void AKraftUnit::RemoveLife(const int _Damage)
{
    Health -= _Damage;
}
