// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Chaos/AABB.h"
#include "Chaos/AABB.h"
#include "GameFramework/Character.h"
#include "Kraft/Player/KraftPlayerState.h"
#include "KraftUnit.generated.h"

#define ATTACK_COOLDOWN 1

class KRAFT_API AWorker;

class KRAFT_API ALeader;

UCLASS()
class KRAFT_API AKraftUnit : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AKraftUnit();

	UFUNCTION()
	void InitUnit(AKraftPlayerState *PlayerStateInit);
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
	virtual void Select(bool IsSelectedNewValue);

	UFUNCTION()
	void MoveToLocation(FVector Location);

	UFUNCTION(Server, Unreliable)
	void AttackUnit(AKraftUnit* Target, int _Damage);
	
	UFUNCTION(Server, Unreliable)
	void AttackWorker(AWorker* Target, int _Damage);
	
	UFUNCTION(Server, Unreliable)
	void AttackLeader(ALeader* Target, int _Damage);
	
	UFUNCTION()
	void RemoveLife(int _Damage);

	UPROPERTY(EditAnywhere, BlueprintReadOnly, ReplicatedUsing=UpdateMaterialColor)
	FLinearColor Color;

	UPROPERTY()
	float CoolDown = 0;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool IsAttacking = false;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, ReplicatedUsing=RepHealth)
	int Health = 100;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, ReplicatedUsing=RepDamage)
	int Damage = 10;

	UFUNCTION(BlueprintCallable)
	void CheckActorInAttackZone(TArray<AActor*> OtherArray, const float DeltaTime);

	// Returns CursorToWorld
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// A decal that projects to the cursor location.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;

	UPROPERTY()
	AKraftPlayerState *PlayerStatePtr;

	UFUNCTION()
	void UpdateMaterialColor();

	UFUNCTION()
	void RepHealth();

	UFUNCTION()
	void RepDamage();
};
