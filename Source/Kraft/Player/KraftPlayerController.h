// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/PlayerController.h"
#include "Kraft/Buildings/Base.h"
#include "Kraft/Buildings/Casern.h"
#include "Kraft/Buildings/GhostBase.h"
#include "Kraft/Buildings/GhostCasern.h"
#include "Kraft/Buildings/GhostResearchCenter.h"
#include "Kraft/Buildings/ResearchCenter.h"
#include "Kraft/Units/KraftUnit.h"
#include "Kraft/Units/Leader.h"
#include "Kraft/Units/Worker.h"
#include "KraftPlayerController.generated.h"

UCLASS()
class AKraftPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AKraftPlayerController();
	virtual void BeginPlay() override;
	virtual void PlayerTick(float DeltaµTime) override;
	virtual void SetupInputComponent() override;

	UPROPERTY(EditDefaultsOnly, Category="Spawning")
	TSubclassOf<AActor> KraftUnitReference;

	UPROPERTY(EditDefaultsOnly, Category="Spawning")
	TSubclassOf<AActor> WorkerReference;

	UPROPERTY(EditDefaultsOnly, Category="Spawning")
	TSubclassOf<AActor> LeaderReference;

	UPROPERTY(EditDefaultsOnly, Category="Spawning")
	TSubclassOf<APawn> CasernReference;

	UPROPERTY(EditDefaultsOnly, Category="Spawning")
	TSubclassOf<APawn> GhostCasernReference;

	UPROPERTY(EditDefaultsOnly, Category="Spawning")
	TSubclassOf<APawn> BaseReference;

	UPROPERTY(EditDefaultsOnly, Category="Spawning")
	TSubclassOf<APawn> GhostBaseReference;

	UPROPERTY(EditDefaultsOnly, Category="Spawning")
	TSubclassOf<APawn> ResearchCenterReference;

	UPROPERTY(EditDefaultsOnly, Category="Spawning")
	TSubclassOf<APawn> GhostResearchCenterReference;
	
	UFUNCTION()
	void InitStarterContent();
	
	UFUNCTION()
	void MouseWheelScrollEventHandler(int32 WheelDirection);

	UFUNCTION()
	void SelectionPressed();

	UFUNCTION()
	void SelectionReleased();

	UFUNCTION(BlueprintCallable)
	bool IsWorkerSelected();

	UFUNCTION(BlueprintCallable)
	bool IsBaseSelected();
	
	UFUNCTION(BlueprintCallable)
	bool IsCasernSelected();
	
	UFUNCTION(BlueprintCallable)
    bool IsResearchCenterSelected();

	// Spawn Ghost Buildings
	UFUNCTION()
	void SpawnGhostBase();

	UFUNCTION()
	void SpawnGhostCasern();

	UFUNCTION()
	void SpawnGhostResearchCenter();

	UFUNCTION()
	void CancelBuildingConstruction();
	
	// Move Ghost Buildings
	UFUNCTION()
	void MoveGhostBase();

	UFUNCTION()
	void MoveGhostCasern();

	UFUNCTION()
    void MoveGhostResearchCenter();

	// Place buildings	
	UFUNCTION()
	void PlaceBase();

	UFUNCTION()
	void PlaceCasern();

	UFUNCTION()
    void PlaceResearchCenter();

	// Check Buildings Spawn
	UFUNCTION()
	void CheckBaseSpawn();

	UFUNCTION()
	void CheckCasernSpawn();

	UFUNCTION()
    void CheckResearchCenterSpawn();

	// Spawn Buildings
	UFUNCTION(Server, Unreliable)
	void SpawnBase(FVector Location);

	UFUNCTION(Server, Unreliable)
	void SpawnCasern(FVector Location);

	UFUNCTION(Server, Unreliable)
    void SpawnResearchCenter(FVector Location);
	
	// Spawn Units
	UFUNCTION()
	void PreSpawnUnit();

	UFUNCTION(Server, Unreliable)
	void SpawnUnit(FVector Location, AActor* OwnerController);

	UFUNCTION()
    void PreSpawnWorker();

	UFUNCTION(Server, Unreliable)
    void SpawnWorker(FVector Location, AKraftPlayerController* OwnerController);

	UFUNCTION()
    void PreSpawnLeader();

	UFUNCTION(Server, Unreliable)
    void SpawnLeader(FVector Location, AActor* OwnerController);

	// Move Units
	UFUNCTION()
	void PreMoveUnits();

	UFUNCTION(Server, Unreliable)
	void MoveUnits(const TArray<AKraftUnit*> &SelectedUnitsToMove, FHitResult Hit);

	UFUNCTION()
	void PreMoveWorkers();

	UFUNCTION(Server, Unreliable)
	void MoveWorkers(const TArray<AWorker*>& SelectedUnitsToMove, FHitResult Hit);

	UFUNCTION(Server, Unreliable)
	void MoveWorker(AWorker *Worker, FVector Location);
	
	UFUNCTION()
	void AssignWorkerToRessourcePit();

	UFUNCTION()
	ABase* GetNearestBase(FVector Location);

	UFUNCTION()
	AWorker *GetNearestWorker(FVector Location);

	UFUNCTION()
    void PreMoveLeaders();

	UFUNCTION(Server, Unreliable)
    void MoveLeaders(const TArray<ALeader*>& SelectedUnitsToMove, FHitResult Hit);

	// Selected Buildings
	UPROPERTY()
	TArray<ABase*> SelectedBases;

	UPROPERTY()
	TArray<ACasern*> SelectedCaserns;
	
	UPROPERTY()
	TArray<AResearchCenter*> SelectedResearchCenters;

	// Selected Units
	UPROPERTY()
	TArray<AKraftUnit*> SelectedUnits;

	UPROPERTY()
	TArray<AWorker*> SelectedWorkers;

	UPROPERTY()
	TArray<ALeader*> SelectedLeaders;

	// Units Pointers
	UPROPERTY()
	AWorker *MovingWorker = nullptr;

	// Buildings Pointers
	UPROPERTY()
	AGhostBase* GhostBase = nullptr;
	
	UPROPERTY()
	AGhostCasern* GhostCasern = nullptr;

	UPROPERTY()
	AGhostResearchCenter* GhostResearchCenter = nullptr;

	UFUNCTION()
	void UpgradeLeaderSpeed();
	
	UPROPERTY(BlueprintReadOnly)
	ALeader* Leader;

	int PlayerIndex;
	FColor PlayerColor;

	DECLARE_DELEGATE_OneParam(FDelegateOneInt, int32);
};