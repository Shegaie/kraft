// Fill out your copyright notice in the Description page of Project Settings.


#include "KraftPlayerState.h"

#include "GeneratedCodeHelpers.h"

void AKraftPlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AKraftPlayerState, Color);
	DOREPLIFETIME(AKraftPlayerState, Faction);
}

void AKraftPlayerState::OnRep_Faction()
{
}

void AKraftPlayerState::OnRep_Color()
{
}

void AKraftPlayerState::SetMineralAmount(int Amount)
{
	MineralAmount = Amount;
}

void AKraftPlayerState::AddMineral(int Amount)
{
	MineralAmount += Amount;
}

bool AKraftPlayerState::RemoveMineral(int Amount)
{
	if (Amount > MineralAmount)
		return false;
	MineralAmount -= Amount;
	return true;
}

int AKraftPlayerState::GetMineralAmount()
{
	return MineralAmount;
}

void AKraftPlayerState::UpgradeUnitsHealth()
{
	UnitsHealth += UnitsHealth * 0.5;
}

void AKraftPlayerState::UpgradeUnitsDamage()
{
	UnitsDamage += UnitsDamage * 0.5;
}
