// Copyright Epic Games, Inc. All Rights Reserved.

#include "KraftPlayerController.h"

#include "KraftPlayerState.h"
#include "Blueprint/UserWidget.h"
#include "Kismet/GameplayStatics.h"
#include "Kraft/Buildings/Base.h"
#include "Kraft/Buildings/ResearchCenter.h"
#include "Kraft/HUDs/KraftHud.h"
#include "Kraft/System/CameraPawn.h"

AKraftPlayerController::AKraftPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}

void AKraftPlayerController::BeginPlay()
{
	if (HasAuthority())
		Possess(GetPawn<ACameraPawn>());
	InitStarterContent();
}

void AKraftPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	if (GhostCasern) {
		if (GhostCasern->IsPlaced)
			CheckCasernSpawn();
		else
			MoveGhostCasern();
	} else if (GhostBase) {
		if (GhostBase->IsPlaced)
			CheckBaseSpawn();
		else
			MoveGhostBase();
	} else if (GhostResearchCenter) {
		if (GhostResearchCenter->IsPlaced)
			CheckResearchCenterSpawn();
		else
			MoveGhostResearchCenter();
	}
}

void AKraftPlayerController::InitStarterContent()
{
	ACameraPawn* PossessedPawn = GetPawn<ACameraPawn>();

	if (!PossessedPawn)
		return;
	const FVector PlayerPosition = PossessedPawn->GetActorLocation() - CAMERA_OFFSET;
	SpawnBase(FVector(PlayerPosition.X, PlayerPosition.Y, 270));
	SpawnWorker(FVector(PlayerPosition.X - 300, PlayerPosition.Y, 220), this);
	SpawnWorker(FVector(PlayerPosition.X - 300, PlayerPosition.Y - 50, 220), this);
	SpawnWorker(FVector(PlayerPosition.X - 300, PlayerPosition.Y + 50, 220), this);
	SpawnLeader(FVector(PlayerPosition.X, PlayerPosition.Y + 300, 220), this);
}

void AKraftPlayerController::MouseWheelScrollEventHandler(int32 WheelDirection)
{
	ACameraPawn* PossessedPawn = GetPawn<ACameraPawn>();

	if (PossessedPawn) {
		PossessedPawn->ZoomCamera(WheelDirection);
	}
}

void AKraftPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	// Camera movement
	InputComponent->BindAction<FDelegateOneInt>("ZoomIn", IE_Released, this,
	                                            &AKraftPlayerController::MouseWheelScrollEventHandler, 1);
	InputComponent->BindAction<FDelegateOneInt>("ZoomOut", IE_Released, this,
	                                            &AKraftPlayerController::MouseWheelScrollEventHandler, -1);

	// Selection
	InputComponent->BindAction("Selection", IE_Pressed, this, &AKraftPlayerController::SelectionPressed);
	InputComponent->BindAction("Selection", IE_Released, this, &AKraftPlayerController::SelectionReleased);

	// Ghost buildings spawn 
	InputComponent->BindAction("SpawnCasern", IE_Released, this, &AKraftPlayerController::SpawnGhostCasern);
	InputComponent->BindAction("SpawnBase", IE_Released, this, &AKraftPlayerController::SpawnGhostBase);
	InputComponent->BindAction("SpawnResearchCenter", IE_Released, this,
	                           &AKraftPlayerController::SpawnGhostResearchCenter);

	InputComponent->BindAction("Quit", IE_Released, this, &AKraftPlayerController::CancelBuildingConstruction);
	// Buildings spawn
	InputComponent->BindAction("PlaceBuilding", IE_Released, this, &AKraftPlayerController::PlaceCasern);
	InputComponent->BindAction("PlaceBuilding", IE_Released, this, &AKraftPlayerController::PlaceBase);
	InputComponent->BindAction("PlaceBuilding", IE_Released, this, &AKraftPlayerController::PlaceResearchCenter);

	// Units spawn
	InputComponent->BindAction("SpawnUnit", IE_Released, this, &AKraftPlayerController::PreSpawnUnit);
	InputComponent->BindAction("SpawnWorker", IE_Released, this, &AKraftPlayerController::PreSpawnWorker);
	InputComponent->BindAction("SpawnLeader", IE_Released, this, &AKraftPlayerController::PreSpawnLeader);

	// Units movement
	InputComponent->BindAction("MoveUnit", IE_Released, this, &AKraftPlayerController::PreMoveUnits);
	InputComponent->BindAction("MoveUnit", IE_Released, this, &AKraftPlayerController::PreMoveWorkers);
	InputComponent->BindAction("MoveUnit", IE_Released, this, &AKraftPlayerController::PreMoveLeaders);

	// Units Action
	InputComponent->BindAction("SelectRessourcePit", IE_Released, this,
	                           &AKraftPlayerController::AssignWorkerToRessourcePit);
}

void AKraftPlayerController::SelectionPressed()
{
	if (GhostCasern || GhostBase || GhostResearchCenter) {
		return;
	}
	AKraftHud* Hud = Cast<AKraftHud>(GetHUD());

	Hud->InitialPoint = Hud->GetMousePos2d();
	Hud->bStartSelecting = true;
}

void AKraftPlayerController::SelectionReleased()
{
	AKraftHud* Hud = Cast<AKraftHud>(GetHUD());

	Hud->bStartSelecting = false;
	SelectedUnits = Hud->FoundUnits;
	SelectedWorkers = Hud->FoundWorkers;
	SelectedLeaders = Hud->FoundLeaders;
	SelectedCaserns = Hud->FoundCaserns;
	SelectedBases = Hud->FoundBases;
	SelectedResearchCenters = Hud->FoundResearchCenters;
}

bool AKraftPlayerController::IsWorkerSelected()
{
	return (SelectedWorkers.Num() != 0);
}

bool AKraftPlayerController::IsBaseSelected()
{
	return (SelectedBases.Num() != 0);
}

bool AKraftPlayerController::IsCasernSelected()
{
	return (SelectedCaserns.Num() != 0);
}

bool AKraftPlayerController::IsResearchCenterSelected()
{
	return (SelectedResearchCenters.Num() != 0);
}

// Spawn Ghost Buildings
void AKraftPlayerController::SpawnGhostBase()
{
	FHitResult Hit;

	if (IsWorkerSelected() && !GhostBase && !GhostCasern) {
		GetHitResultUnderCursor(ECC_Visibility, false, Hit);
		AGhostBase* SpawnedBase = Cast<AGhostBase>(GetWorld()->SpawnActor<APawn>(
			GhostBaseReference, FVector(Hit.Location.X, Hit.Location.Y, 270), FRotator(0, 0, 0)));
		if (SpawnedBase) {
			GhostBase = SpawnedBase;
		}
	}
}

void AKraftPlayerController::SpawnGhostCasern()
{
	FHitResult Hit;

	if (IsWorkerSelected() && !GhostCasern && !GhostBase) {
		GetHitResultUnderCursor(ECC_Visibility, false, Hit);
		AGhostCasern* SpawnedGhostCasern = Cast<AGhostCasern>(GetWorld()->SpawnActor<APawn>(
			GhostCasernReference, FVector(Hit.Location.X, Hit.Location.Y, 220), FRotator(0, 0, 0)));
		if (SpawnedGhostCasern) {
			GhostCasern = SpawnedGhostCasern;
		}
	}
}

void AKraftPlayerController::SpawnGhostResearchCenter()
{
	FHitResult Hit;

	if (IsWorkerSelected() && !GhostCasern && !GhostBase && !GhostResearchCenter) {
		GetHitResultUnderCursor(ECC_Visibility, false, Hit);
		AGhostResearchCenter* SpawnedGhostResearchCenter = Cast<AGhostResearchCenter>(GetWorld()->SpawnActor<APawn>(
			GhostResearchCenterReference, FVector(Hit.Location.X, Hit.Location.Y, 250), FRotator(0, 0, 0)));
		if (SpawnedGhostResearchCenter) {
			GhostResearchCenter = SpawnedGhostResearchCenter;
		}
	}
}

void AKraftPlayerController::CancelBuildingConstruction()
{
	if (GhostCasern && !GhostCasern->IsPlaced) {
		GhostCasern->ConditionalBeginDestroy();
		GhostCasern = nullptr;
	} else if (GhostBase && !GhostBase->IsPlaced) {
		GhostBase->ConditionalBeginDestroy();
		GhostBase = nullptr;
	} else if (GhostResearchCenter && !GhostResearchCenter->IsPlaced) {
		GhostResearchCenter->ConditionalBeginDestroy();
		GhostResearchCenter = nullptr;
	}
}

// Move Ghost Buildings
void AKraftPlayerController::MoveGhostBase()
{
	if (GhostBase) {
		FHitResult Hit;
		GetHitResultUnderCursor(ECC_Visibility, false, Hit);
		GhostBase->SetActorLocation(FVector(Hit.Location.X, Hit.Location.Y, 270));
	}
}

void AKraftPlayerController::MoveGhostCasern()
{
	if (GhostCasern) {
		FHitResult Hit;
		GetHitResultUnderCursor(ECC_Visibility, false, Hit);
		GhostCasern->SetActorLocation(FVector(Hit.Location.X, Hit.Location.Y, 220));
	}
}

void AKraftPlayerController::MoveGhostResearchCenter()
{
	if (GhostResearchCenter) {
		FHitResult Hit;
		GetHitResultUnderCursor(ECC_Visibility, false, Hit);
		GhostResearchCenter->SetActorLocation(FVector(Hit.Location.X, Hit.Location.Y, 250));
	}
}

// Place buildings
void AKraftPlayerController::PlaceBase()
{
	FHitResult Hit;

	if (GhostBase && GhostBase->IsPlaceable) {
		GetHitResultUnderCursor(ECC_Visibility, false, Hit);
		if (SelectedWorkers.Num() > 0) {
			MoveWorker(GetNearestWorker(FVector(Hit.Location.X, Hit.Location.Y, 220)),
			           FVector(Hit.Location.X, Hit.Location.Y, 220));
		}
		GhostBase->IsPlaced = true;
	}
}

void AKraftPlayerController::PlaceCasern()
{
	FHitResult Hit;

	if (GhostCasern && GhostCasern->IsPlaceable) {
		GetHitResultUnderCursor(ECC_Visibility, false, Hit);
		if (SelectedWorkers.Num() > 0) {
			MoveWorker(GetNearestWorker(FVector(Hit.Location.X, Hit.Location.Y, 220)),
			           FVector(Hit.Location.X, Hit.Location.Y, 220));
		}
		GhostCasern->IsPlaced = true;
	}
}

void AKraftPlayerController::PlaceResearchCenter()
{
	FHitResult Hit;

	if (GhostResearchCenter && GhostResearchCenter->IsPlaceable) {
		GetHitResultUnderCursor(ECC_Visibility, false, Hit);
		if (SelectedWorkers.Num() > 0) {
			MoveWorker(GetNearestWorker(FVector(Hit.Location.X, Hit.Location.Y, 220)),
			           FVector(Hit.Location.X, Hit.Location.Y, 220));
		}
		GhostResearchCenter->IsPlaced = true;
	}
}

// Check Buildings Spawn
void AKraftPlayerController::CheckBaseSpawn()
{
	AWorker* NearestWorker = GetNearestWorker(GhostBase->GetActorLocation());

	if ((NearestWorker->GetActorLocation() - GhostBase->GetActorLocation()).Size() < 300) {
		MoveWorker(NearestWorker, NearestWorker->GetActorLocation());
		const FVector LocToSpawn = {GhostBase->GetActorLocation().X, GhostBase->GetActorLocation().Y, 270};
		SpawnBase(LocToSpawn);
		GhostBase->ConditionalBeginDestroy();
		GhostBase = nullptr;
	}
}

void AKraftPlayerController::CheckCasernSpawn()
{
	AWorker* NearestWorker = GetNearestWorker(GhostCasern->GetActorLocation());

	if ((NearestWorker->GetActorLocation() - GhostCasern->GetActorLocation()).Size() < 130) {
		MoveWorker(NearestWorker, NearestWorker->GetActorLocation());
		const FVector LocToSpawn = {GhostCasern->GetActorLocation().X, GhostCasern->GetActorLocation().Y, 220};
		SpawnCasern(LocToSpawn);
		GhostCasern->ConditionalBeginDestroy();
		GhostCasern = nullptr;
	}
}

void AKraftPlayerController::CheckResearchCenterSpawn()
{
	AWorker* NearestWorker = GetNearestWorker(GhostResearchCenter->GetActorLocation());

	if ((NearestWorker->GetActorLocation() - GhostResearchCenter->GetActorLocation()).Size() < 200) {
		MoveWorker(NearestWorker, NearestWorker->GetActorLocation());
		const FVector LocToSpawn = {
			GhostResearchCenter->GetActorLocation().X, GhostResearchCenter->GetActorLocation().Y, 250
		};
		SpawnResearchCenter(LocToSpawn);
		GhostResearchCenter->ConditionalBeginDestroy();
		GhostResearchCenter = nullptr;
	}
}

// Spawn Buildings
void AKraftPlayerController::SpawnBase_Implementation(const FVector Location)
{
	if (!HasAuthority())
		return;
	ABase* SpawnedBase = Cast<ABase>(GetWorld()->SpawnActor<APawn>(BaseReference, Location, FRotator(0, 0, 0)));
	if (SpawnedBase) {
		SpawnedBase->Init(GetPlayerState<AKraftPlayerState>());
	}
}

void AKraftPlayerController::SpawnCasern_Implementation(const FVector Location)
{
	if (!HasAuthority())
		return;
	ACasern* SpawnedCasern = Cast<ACasern>(GetWorld()->SpawnActor<APawn>(CasernReference, Location, FRotator(0, 0, 0)));
	if (SpawnedCasern) {
		SpawnedCasern->Init(GetPlayerState<AKraftPlayerState>());
	}
}

void AKraftPlayerController::SpawnResearchCenter_Implementation(const FVector Location)
{
	if (!HasAuthority())
		return;
	AResearchCenter* SpawnedResearchCenter = Cast<AResearchCenter>(
		GetWorld()->SpawnActor<APawn>(ResearchCenterReference, Location, FRotator(0, 0, 0)));
	if (SpawnedResearchCenter) {
		SpawnedResearchCenter->Init(GetPlayerState<AKraftPlayerState>());
	}
}

// Spawn Units
void AKraftPlayerController::PreSpawnUnit()
{
	FHitResult Hit;
	GetHitResultUnderCursor(ECC_Visibility, false, Hit);
	SpawnUnit(Hit.Location, this);
}

void AKraftPlayerController::SpawnUnit_Implementation(FVector Location, AActor* OwnerController)
{
	if (!HasAuthority())
		return;
	AKraftUnit* SpawnedActor = Cast<AKraftUnit>(
		GetWorld()->SpawnActor<AActor>(KraftUnitReference, Location, FRotator(0, 0, 0)));
	if (SpawnedActor) {
		SpawnedActor->SetOwner(OwnerController);
		SpawnedActor->InitUnit(GetPlayerState<AKraftPlayerState>());
	}
}

void AKraftPlayerController::PreSpawnWorker()
{
	FHitResult Hit;
	GetHitResultUnderCursor(ECC_Visibility, false, Hit);
	SpawnWorker(Hit.Location, this);
}

void AKraftPlayerController::SpawnWorker_Implementation(FVector Location, AKraftPlayerController* OwnerController)
{
	if (!HasAuthority())
		return;
	AWorker* SpawnedWorker = nullptr;
	while (!SpawnedWorker) {
		SpawnedWorker = Cast<AWorker>(GetWorld()->SpawnActor<AActor>(WorkerReference, Location, FRotator(0, 0, 0)));
		Location.X -= 60;
	}
	SpawnedWorker->SetOwner(OwnerController);
	SpawnedWorker->InitUnit(GetPlayerState<AKraftPlayerState>());
}

void AKraftPlayerController::PreSpawnLeader()
{
	FHitResult Hit;
	GetHitResultUnderCursor(ECC_Visibility, false, Hit);
	SpawnLeader(Hit.Location, this);
}

void AKraftPlayerController::SpawnLeader_Implementation(const FVector Location, AActor* OwnerController)
{
	if (!HasAuthority())
		return;
	ALeader* SpawnedLeader = Cast<ALeader
	>(GetWorld()->SpawnActor<AActor>(LeaderReference, Location, FRotator(0, 0, 0)));
	if (SpawnedLeader) {
		SpawnedLeader->InitUnit(GetPlayerState<AKraftPlayerState>());
		Leader = SpawnedLeader;
	}
}

// Move Units
void AKraftPlayerController::PreMoveUnits()
{
	if (SelectedUnits.Num() > 0) {
		FHitResult Hit;
		GetHitResultUnderCursor(ECC_Visibility, false, Hit);
		MoveUnits(SelectedUnits, Hit);
	}
}

void AKraftPlayerController::MoveUnits_Implementation(const TArray<AKraftUnit*>& SelectedUnitsToMove,
                                                      const FHitResult Hit)
{
	if (!HasAuthority())
		return;

	for (AKraftUnit *Units: SelectedUnitsToMove) {
		if (IsValid(Units)) {
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::White, FString::Printf(TEXT("Unit name: %s"), ToCStr(Units->GetName())));
			Units->MoveToLocation(Hit.Location);
		}
	}
}

void AKraftPlayerController::PreMoveWorkers()
{
	if (SelectedWorkers.Num() > 0) {
		FHitResult Hit;
		GetHitResultUnderCursor(ECC_Visibility, false, Hit);
		ARessourcePit* RessourcePitUnderCursor = Cast<ARessourcePit>(Hit.GetActor());
		if (RessourcePitUnderCursor == nullptr) {
			GetHitResultUnderCursor(ECC_Visibility, false, Hit);
			MoveWorkers(SelectedWorkers, Hit);
		}
	}
}

void AKraftPlayerController::MoveWorkers_Implementation(const TArray<AWorker*>& SelectedUnitsToMove,
                                                        const FHitResult Hit)
{
	if (!HasAuthority())
		return;
	for (AWorker *Units: SelectedUnitsToMove) {
		if (IsValid(Units))
			Units->MoveToLocation(Hit.Location);
	}
}

void AKraftPlayerController::MoveWorker_Implementation(AWorker* Worker, const FVector Location)
{
	if (!HasAuthority())
		return;
	if (Worker) {
		Worker->MoveToLocation(Location);
	}
}

AWorker* AKraftPlayerController::GetNearestWorker(FVector Location)
{
	AWorker* NearestWorker = SelectedWorkers[0];
	for (AWorker* Worker : SelectedWorkers) {
		if ((Worker->GetActorLocation() - Location).Size() < (NearestWorker->GetActorLocation() - Location).Size()) {
			NearestWorker = Worker;
		}
	}
	return NearestWorker;
}

void AKraftPlayerController::PreMoveLeaders()
{
	if (SelectedLeaders.Num() > 0) {
		FHitResult Hit;
		GetHitResultUnderCursor(ECC_Visibility, false, Hit);
		MoveLeaders(SelectedLeaders, Hit);
	}
}

void AKraftPlayerController::MoveLeaders_Implementation(const TArray<ALeader*>& SelectedLeadersToMove,
                                                        const FHitResult Hit)
{
	if (!HasAuthority())
		return;

	for (ALeader* Units: SelectedLeadersToMove) {
		if (IsValid(Units))
			Units->MoveToLocation(Hit.Location);
	}
}

void AKraftPlayerController::AssignWorkerToRessourcePit()
{
	FHitResult Hit;
	GetHitResultUnderCursor(ECC_Visibility, false, Hit);
	ARessourcePit* RessourcePitUnderCursor = Cast<ARessourcePit>(Hit.GetActor());

	if (RessourcePitUnderCursor) {
		ABase* NearestBase = GetNearestBase(RessourcePitUnderCursor->GetActorLocation());
		for (auto Worker : SelectedWorkers) {
			Worker->SetActiveRessourcePit(RessourcePitUnderCursor);
			Worker->SetActiveBase(NearestBase);
			MoveWorker(Worker, RessourcePitUnderCursor->GetActorLocation());
			Worker->SetWorkerState(EWorkerState::MovingToRessourcePit);
			Worker->SetWorkerTask(EWorkerTask::MiningRessource);
		}
	}
}

ABase* AKraftPlayerController::GetNearestBase(FVector Location)
{
	TSubclassOf<ABase> ClassToFind = ABase::StaticClass();
	TArray<AActor*> FoundBases;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ClassToFind, FoundBases);
	FoundBases.RemoveAll([this](AActor* Val)
	{
		ABase* Base = Cast<ABase>(Val);
		return Base->Color != GetPlayerState<AKraftPlayerState>()->GetColor();
	});
	if (FoundBases.Num() == 0)
		return nullptr;
	ABase* NearestBase = Cast<ABase>(FoundBases[0]);
	for (AActor* BaseActor : FoundBases) {
		ABase* Base = Cast<ABase>(BaseActor);
		if ((Base->GetActorLocation() - Location).Size() < (NearestBase->GetActorLocation() - Location).Size())
			NearestBase = Base;
	}
	return NearestBase;
}

void AKraftPlayerController::UpgradeLeaderSpeed()
{
	Leader->SetMaxSpeed(Leader->MaxSpeed + 100);
}
