// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "KraftPlayerState.generated.h"

UCLASS()
class KRAFT_API AKraftPlayerState : public APlayerState
{
	GENERATED_BODY()

public:
	UFUNCTION()
	FLinearColor GetColor() const { return Color; }

	UFUNCTION()
	void SetColor(const FLinearColor NewValue) { Color = NewValue; }

	UFUNCTION()
	FString GetFaction() const { return Faction; }

	UFUNCTION()
	void SetFaction(const FString NewValue) { Faction = NewValue; }

	UFUNCTION()
	void OnRep_Faction();

	UFUNCTION()
	void OnRep_Color();

	UFUNCTION(BlueprintCallable)
	void SetMineralAmount(int Amount);

	UFUNCTION(BlueprintCallable)
	void AddMineral(int Amount);
	
	UFUNCTION(BlueprintCallable)
	bool RemoveMineral(int Amount);

	UFUNCTION(BlueprintCallable)
	int GetMineralAmount();

	UFUNCTION(BlueprintCallable)
    void UpgradeUnitsHealth();

	UFUNCTION(BlueprintCallable)
    void UpgradeUnitsDamage();

	UFUNCTION(BlueprintCallable)
	int GetUnitsHealth() const { return UnitsHealth; }

	UFUNCTION(BlueprintCallable)
	int GetUnitsDamage() const { return UnitsDamage; }
	
	UPROPERTY(BlueprintReadWrite, ReplicatedUsing=OnRep_Color)
	FLinearColor Color;

private:
	UPROPERTY(ReplicatedUsing=OnRep_Faction)
	FString Faction;

	UPROPERTY()
	int MineralAmount = 150;

	UPROPERTY()
	int UnitsHealth = 100;

	UPROPERTY()
	int UnitsDamage = 10;
};
