// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/Pawn.h"
#include "Kraft/Player/KraftPlayerState.h"

#include "Base.generated.h"

UCLASS()
class KRAFT_API ABase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ABase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
    void Init(AKraftPlayerState* PlayerStateInit);

	UFUNCTION()
    void UpdateMaterialColor();

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* Mesh;

	UPROPERTY(VisibleAnywhere)
	UMaterial* Material;
	
	UFUNCTION(BlueprintCallable)
    void Select(bool NewValue);

	UPROPERTY()
	AKraftPlayerState *PlayerStatePtr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, ReplicatedUsing=UpdateMaterialColor)
	FLinearColor Color;
	
	UPROPERTY(VisibleAnywhere)
	bool IsSelected;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;
};
