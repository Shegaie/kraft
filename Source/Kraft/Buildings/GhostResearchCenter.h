// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "GhostResearchCenter.generated.h"

UCLASS()
class KRAFT_API AGhostResearchCenter : public APawn
{
	GENERATED_BODY()
	
protected:
	virtual void BeginPlay() override;

public:
	
	AGhostResearchCenter();
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
		
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	UStaticMeshComponent* Mesh;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	UMaterial* Material;

	UFUNCTION(BlueprintCallable)
    void SetPlaceable(bool NewValue);
	
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	bool IsPlaceable;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	bool IsPlaced = false;
};
