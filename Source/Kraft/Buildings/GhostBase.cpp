// Fill out your copyright notice in the Description page of Project Settings.


#include "GhostBase.h"

// Sets default values
AGhostBase::AGhostBase()
{
	// Set this pawn to call Tick() every frame.
	PrimaryActorTick.bCanEverTick = true;

	IsPlaceable = true;

	// Create, load and set mesh
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("GHOST BUILDING MESH"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> BuildingStaticMesh(TEXT("StaticMesh'/Game/Geometry/Meshes/1M_Cube'"));
	if (BuildingStaticMesh.Object) {
		Mesh->SetStaticMesh(BuildingStaticMesh.Object);
	}

	// Set mesh collisions
	Mesh->SetCollisionResponseToAllChannels(ECR_Overlap);

	// Create, load and set material
	Material = CreateDefaultSubobject<UMaterial>(TEXT("GHOST MATERIAL"));
	static ConstructorHelpers::FObjectFinder<UMaterial> LoadedMaterial(TEXT("Material'/Game/Geometry/Meshes/GhostMaterial'"));
	if (LoadedMaterial.Object) {
		Material = LoadedMaterial.Object;
		Mesh->SetMaterial(0, Material);
	}
}

// Called when the game starts or when spawned
void AGhostBase::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AGhostBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AGhostBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AGhostBase::SetPlaceable(const bool NewValue)
{
	IsPlaceable = NewValue;
}


