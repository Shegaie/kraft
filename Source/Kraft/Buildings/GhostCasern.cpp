// Fill out your copyright notice in the Description page of Project Settings.

#include "GhostCasern.h"

AGhostCasern::AGhostCasern()
{
	PrimaryActorTick.bCanEverTick = true;
	IsPlaceable = true;

	// Create, load and set mesh
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("GHOST BUILDING MESH"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> BuildingStaticMesh(TEXT("StaticMesh'/Game/Geometry/Meshes/1M_Cube'"));
	if (BuildingStaticMesh.Object) {
		Mesh->SetStaticMesh(BuildingStaticMesh.Object);
	}

	// Set mesh collisions
	Mesh->SetCollisionResponseToAllChannels(ECR_Overlap);

	// Create, load and set material
	Material = CreateDefaultSubobject<UMaterial>(TEXT("GHOST MATERIAL"));
	static ConstructorHelpers::FObjectFinder<UMaterial> LoadedMaterial(TEXT("Material'/Game/Geometry/Meshes/GhostMaterial'"));
	if (LoadedMaterial.Object) {
		Material = LoadedMaterial.Object;
		Mesh->SetMaterial(0, Material);
	}
}

void AGhostCasern::BeginPlay()
{
	Super::BeginPlay();
}

void AGhostCasern::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AGhostCasern::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AGhostCasern::SetPlaceable(const bool NewValue)
{
	IsPlaceable = NewValue;
}
