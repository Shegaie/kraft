// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Kraft/Player/KraftPlayerState.h"
#include "ResearchCenter.generated.h"

UCLASS()
class KRAFT_API AResearchCenter : public APawn
{
	GENERATED_BODY()
protected:
	
	virtual void BeginPlay() override;

public:
	
	AResearchCenter();
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	UFUNCTION()
    void Init(AKraftPlayerState* PlayerStateInit);

	UFUNCTION()
    void UpdateMaterialColor();

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* Mesh;

	UPROPERTY(VisibleAnywhere)
	UMaterial* Material;
	
	UFUNCTION(BlueprintCallable)
    void Select(bool NewValue);

	UPROPERTY(VisibleAnywhere)
	bool IsSelected;
	
	UPROPERTY()
	AKraftPlayerState *PlayerStatePtr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, ReplicatedUsing=UpdateMaterialColor)
	FLinearColor Color;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;
};
