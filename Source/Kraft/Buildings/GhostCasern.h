// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "GhostCasern.generated.h"

UCLASS()
class KRAFT_API AGhostCasern : public APawn
{
	GENERATED_BODY()
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Sets default values for this pawn's properties
	AGhostCasern();
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	UStaticMeshComponent* Mesh;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	UMaterial* Material;

	UFUNCTION(BlueprintCallable)
	void SetPlaceable(bool NewValue);
	
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	bool IsPlaceable;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	bool IsPlaced = false;
};
