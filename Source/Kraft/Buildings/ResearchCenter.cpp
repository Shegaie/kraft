// Fill out your copyright notice in the Description page of Project Settings.

#include "ResearchCenter.h"
#include "GeneratedCodeHelpers.h"
#include "Components/DecalComponent.h"

AResearchCenter::AResearchCenter()
{
	PrimaryActorTick.bCanEverTick = true;
	IsSelected = false;

	// Create, load and set mesh
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BUILDING MESH"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> BuildingStaticMesh(TEXT("StaticMesh'/Game/Geometry/Meshes/1M_Cube'"));
	if (BuildingStaticMesh.Object) {
		Mesh->SetStaticMesh(BuildingStaticMesh.Object);
	}

	// Set mesh collisions
	Mesh->SetCollisionResponseToAllChannels(ECR_Block);

	// Create, load and set material
	Material = CreateDefaultSubobject<UMaterial>(TEXT("BUILDING MATERIAL"));
	static ConstructorHelpers::FObjectFinder<UMaterial> LoadedMaterial(TEXT("Material'/Game/Geometry/Meshes/BuildingMaterial'"));
	if (LoadedMaterial.Object) {
		Material = LoadedMaterial.Object;
		Mesh->SetMaterial(0, Material);
	}

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Kraft_BP/Blueprints/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded()) {
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(87.0f, 87.0f, 87.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());
	CursorToWorld->SetVisibility(false);
}

void AResearchCenter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AResearchCenter, Color);
}

void AResearchCenter::Init(AKraftPlayerState* PlayerStateInit)
{
	if (HasAuthority() && GetOwner()) {
		PlayerStatePtr = PlayerStateInit;
		FString PlayerName = PlayerStatePtr->GetPlayerName();
		Color = PlayerStatePtr->GetColor();
	}
}

void AResearchCenter::UpdateMaterialColor()
{
	UMaterialInterface* MaterialInterface = Mesh->GetMaterial(0);
	UMaterialInstanceDynamic* DynamicMaterial = Mesh->CreateDynamicMaterialInstance(0, MaterialInterface);
	if (DynamicMaterial != nullptr) {
		DynamicMaterial->SetVectorParameterValue("BaseColor", Color);
		Mesh->SetMaterial(0, DynamicMaterial);
	}
}

void AResearchCenter::BeginPlay()
{
	Super::BeginPlay();
}

void AResearchCenter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AResearchCenter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AResearchCenter::Select(const bool NewValue)
{
	IsSelected = NewValue;
	CursorToWorld->SetVisibility(NewValue);
	CursorToWorld->SetWorldLocation(GetActorLocation());
}


