// Copyright Epic Games, Inc. All Rights Reserved.

#include "Kraft.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Kraft, "Kraft" );

DEFINE_LOG_CATEGORY(LogKraft)
 