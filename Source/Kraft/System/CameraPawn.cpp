// Fill out your copyright notice in the Description page of Project Settings.

#include "CameraPawn.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kraft/Player/KraftPlayerController.h"

// Sets default values
ACameraPawn::ACameraPawn()
{
    // Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
    bReplicates = false;

    RootScene = CreateDefaultSubobject<USceneComponent>(TEXT("RootScene"));
    RootComponent = RootScene;

    SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
    SpringArm->SetupAttachment(RootScene);
    SpringArm->bDoCollisionTest = false;
    SpringArm->SetWorldRotation(FRotator(-50, 0, 0));
    SpringArm->SetRelativeLocation(CAMERA_OFFSET);

    Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
    Camera->SetupAttachment(SpringArm);
}

// Called when the game starts or when spawned
void ACameraPawn::BeginPlay()
{
    Super::BeginPlay();
    if (GetWorld()->GetFirstPlayerController()) {
        PC = CastChecked<AKraftPlayerController>(GetWorld()->GetFirstPlayerController());
        PC->GetViewportSize(ScreenSizeX, ScreenSizeY);
    }
}

// Called every frame
void ACameraPawn::Tick(const float DeltaTime)
{
    Super::Tick(DeltaTime);
    if (GetWorld()->GetFirstPlayerController()) {
        MoveCamera(GetCameraPanDirection());
    }
}

FVector ACameraPawn::GetCameraPanDirection() const
{
    float MousePosX;
    float MousePosY;
    float CamDirectionX = 0;
    float CamDirectionY = 0;
    const bool IsMouse = PC->GetMousePosition(MousePosX, MousePosY);

    if (IsMouse && MousePosX >= 0 && MousePosY >= 0 && MousePosX <= ScreenSizeX && MousePosY <= ScreenSizeY) {
        if (MousePosX <= CameraMovementMargin) {
            CamDirectionY = -1;
        }
        if (MousePosY <= CameraMovementMargin) {
            CamDirectionX = 1;
        }
        if (MousePosX >= ScreenSizeX - CameraMovementMargin) {
            CamDirectionY = 1;
        }
        if (MousePosY >= ScreenSizeY - CameraMovementMargin) {
            CamDirectionX = -1;
        }
    }
    return FVector(CamDirectionX, CamDirectionY, 0);
}

void ACameraPawn::MoveCamera(const FVector Dir)
{
    if (Dir != FVector::ZeroVector) {
        AddActorLocalOffset(Dir * (CamSpeed + ZoomStep * 2));
    }
}

// Called to bind functionality to input
void ACameraPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void ACameraPawn::ZoomCamera(int32 Direction)
{
    const FRotator SpringArmRotation = SpringArm->GetRelativeRotation();
    const FVector RootComponentLocation = RootComponent->GetRelativeLocation();

    if ((ZoomStep + 1 < 5 && Direction == 1) ||
        (ZoomStep > 1 && Direction == -1)) {
        ZoomStep += Direction;
        Direction *= 8;
        SpringArm->SetRelativeRotation(FRotator(SpringArmRotation.Pitch + Direction, SpringArmRotation.Yaw,
                                                SpringArmRotation.Roll));
        RootComponent->SetRelativeLocation(RootComponentLocation + FVector(0, 0, - Direction * 10));
    }
}
