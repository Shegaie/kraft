// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/Pawn.h"
#include "CameraPawn.generated.h"

#define CAMERA_OFFSET FVector(-200, 0, 500)

class AKraftPlayerController;
class USpringArmComponent;
UCLASS()

class KRAFT_API ACameraPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ACameraPawn();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY()
	USceneComponent* RootScene;

	UPROPERTY()
	USpringArmComponent* SpringArm;

	UPROPERTY()
	UCameraComponent* Camera;

	UPROPERTY()
	AKraftPlayerController* PC;

	UFUNCTION()
	FVector GetCameraPanDirection() const;

	UFUNCTION()
	void ZoomCamera(int32 Direction);

	UFUNCTION()
	void MoveCamera(FVector Dir);

	UPROPERTY()
	int32 ZoomStep = 1;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="CameraProperties")
	float CamSpeed = 15;

	UPROPERTY()
	float CameraMovementMargin = 15;

	UPROPERTY()
	int32 ScreenSizeX;

	UPROPERTY()
	int32 ScreenSizeY;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};
