// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "GameFramework/PlayerController.h"
#include "KraftGameMode.generated.h"

/**
 * 
 */
UCLASS()
class KRAFT_API AKraftGameMode : public AGameMode
{
	GENERATED_BODY()
public:

	TArray<FLinearColor> PlayersColor = {FLinearColor::Green, FLinearColor::Blue, FLinearColor::Yellow, FLinearColor::Red};
	// List of PlayerControllers
	UPROPERTY(BlueprintReadOnly)
	TArray<class APlayerController*> PlayerControllerList;

	// Overriding the PostLogin function
	UFUNCTION()
	virtual void PostLogin(APlayerController* NewPlayer) override;
};
