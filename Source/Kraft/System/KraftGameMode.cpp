// Fill out your copyright notice in the Description page of Project Settings.


#include "KraftGameMode.h"

#include "Blueprint/UserWidget.h"
#include "Kraft/Player/KraftPlayerController.h"
#include "Kraft/Player/KraftPlayerState.h"

void AKraftGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);
	if (!HasAuthority())
		return;
	PlayerControllerList.Add(NewPlayer);
	FString PlayerName = "player_";
	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::White, FString::Printf(TEXT("NEW PLAYER")));
	PlayerName.Append(FString::FromInt(GetNumPlayers()));
	if (NewPlayer->GetPlayerState<AKraftPlayerState>()) {
		NewPlayer->GetPlayerState<AKraftPlayerState>()->SetPlayerName(PlayerName);
		NewPlayer->GetPlayerState<AKraftPlayerState>()->SetFaction("HIPPIE");
		if (GetNumPlayers() != 0)
			NewPlayer->GetPlayerState<AKraftPlayerState>()->SetColor(PlayersColor[GetNumPlayers() - 1]);
	}
}
