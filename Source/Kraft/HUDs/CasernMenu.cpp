// Fill out your copyright notice in the Description page of Project Settings.


#include "CasernMenu.h"

void UCasernMenu::NativeConstruct()
{
	Super::NativeConstruct();

	PlayerControllerPtr = GetOwningPlayer<AKraftPlayerController>();
    
	SpawnUnitButton->OnClicked.AddUniqueDynamic(this, &UCasernMenu::OnSpawnUnitButtonClicked);
	PlayerStatePtr = PlayerControllerPtr->GetPlayerState<AKraftPlayerState>();
}

void UCasernMenu::OnSpawnUnitButtonClicked()
{
	ACasern *Casern = PlayerControllerPtr->SelectedCaserns[0];
	FVector PosToSpawn = Casern->GetActorLocation();

	// Offset to spawn in front of base
	PosToSpawn.X -= 100;
	PosToSpawn.Z = 220;

	if(PlayerStatePtr->GetMineralAmount() >= UNIT_PRICE) {
		PlayerControllerPtr->SpawnUnit(PosToSpawn, PlayerControllerPtr);
		PlayerStatePtr->RemoveMineral(UNIT_PRICE);
	}
}