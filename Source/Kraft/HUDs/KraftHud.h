// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/HUD.h"
#include "Kraft/Buildings/Base.h"
#include "Kraft/Buildings/Casern.h"
#include "Kraft/Buildings/ResearchCenter.h"
#include "Kraft/Units/KraftUnit.h"
#include "Kraft/Units/Worker.h"
#include "Kraft/Units/Leader.h"
#include "KraftHud.generated.h"

UCLASS()
class KRAFT_API AKraftHud : public AHUD
{
	GENERATED_BODY()

public:
	virtual void DrawHUD() override;

	FVector2D GetMousePos2d();

	bool bStartSelecting = false;
	
	FVector2D InitialPoint;
	FVector2D CurrentPoint;

	// Search Units
	UFUNCTION()
	void SearchUnits();

	UFUNCTION()
    void SearchWorkers();

	UFUNCTION()
    void SearchLeaders();

	// Select Units
	UFUNCTION()
    void SelectAllUnits(const bool NewSelection);

	UFUNCTION()
    void SelectAllWorkers(const bool NewSelection);

	UFUNCTION()
	void SelectAllLeaders(const bool NewSelection);
	
	// Found Units
	UPROPERTY()
	TArray<AKraftUnit *> FoundUnits;

	UPROPERTY()
	TArray<AWorker *> FoundWorkers;

	UPROPERTY()
	TArray<ALeader *> FoundLeaders;

	// Search Buildings
	UFUNCTION()
    void SearchCaserns();

	UFUNCTION()
	void SearchBases();

	UFUNCTION()
    void SearchResearchCenters();
	
	// Select Buildings
	UFUNCTION()
    void SelectAllCaserns(const bool NewSelection);

	UFUNCTION()
    void SelectAllBases(const bool NewSelection);

	UFUNCTION()
    void SelectAllResearchCenters(const bool NewSelection);

	// Found Buildings
	UPROPERTY()
	TArray<ACasern *> FoundCaserns;

	UPROPERTY()
	TArray<ABase *> FoundBases;

	UPROPERTY()
	TArray<AResearchCenter *> FoundResearchCenters;
};
