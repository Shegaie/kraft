// Fill out your copyright notice in the Description page of Project Settings.

#include "ResearchCenterMenu.h"

void UResearchCenterMenu::NativeConstruct()
{
	Super::NativeConstruct();

	PlayerControllerPtr = GetOwningPlayer<AKraftPlayerController>();
	PlayerStatePtr = PlayerControllerPtr->GetPlayerState<AKraftPlayerState>();
	
	UpgradeUnitsAttackButton->OnClicked.AddUniqueDynamic(this, &UResearchCenterMenu::OnUpgradeUnitsAttackButtonClicked);
	UpgradeUnitsHealthButton->OnClicked.AddUniqueDynamic(this, &UResearchCenterMenu::OnUpgradeUnitsHealthButtonClicked);
	UpgradeLeaderSpeedButton->OnClicked.AddUniqueDynamic(this, &UResearchCenterMenu::OnUpgradeLeaderSpeedButtonClicked);

	SetText(UpgradeUnitsAttackText, "Upgrade Damage: ", UpgradeUnitsAttackPrice,
                UpgradeUnitsAttackNumber, MAX_UPGRADE_NUMBER);
	SetText(UpgradeUnitsHealthText, "Upgrade Health: ", UpgradeUnitsHealthPrice,
                UpgradeUnitsHealthNumber, MAX_UPGRADE_NUMBER);
	SetText(UpgradeLeaderSpeedText, "Upgrade Leader Speed: ", UpgradeLeaderSpeedPrice,
                UpgradeLeaderSpeedNumber, LEADER_SPEED_MAX_UPGRADE_NUMBER);
}

void UResearchCenterMenu::OnUpgradeUnitsAttackButtonClicked()
{
	if (UpgradeUnitsAttackNumber + 1 > MAX_UPGRADE_NUMBER) {
		return;
	}
	if (PlayerStatePtr->GetMineralAmount() < UpgradeUnitsAttackPrice) {
		return;
	}
	UpgradeUnitsAttackNumber += 1;
	SetText(UpgradeUnitsAttackText, "Upgrade Damage: ", UpgradeUnitsAttackPrice,
		UpgradeUnitsAttackNumber, MAX_UPGRADE_NUMBER);
	PlayerStatePtr->UpgradeUnitsDamage();
	PlayerStatePtr->RemoveMineral(UpgradeUnitsAttackPrice);
	UpgradeUnitsAttackPrice += 100;
}

void UResearchCenterMenu::OnUpgradeUnitsHealthButtonClicked()
{
	if (UpgradeUnitsHealthNumber + 1 > MAX_UPGRADE_NUMBER) {
		return;
	}
	if (PlayerStatePtr->GetMineralAmount() < UpgradeUnitsHealthPrice) {
		return;
	}
	UpgradeUnitsHealthNumber += 1;
	SetText(UpgradeUnitsHealthText, "Upgrade Health: ", UpgradeUnitsHealthPrice,
		UpgradeUnitsHealthNumber, MAX_UPGRADE_NUMBER);
	PlayerStatePtr->UpgradeUnitsHealth();
	PlayerStatePtr->RemoveMineral(UpgradeUnitsHealthPrice);
	UpgradeUnitsHealthPrice += 100;
}

void UResearchCenterMenu::OnUpgradeLeaderSpeedButtonClicked()
{	
	if (UpgradeLeaderSpeedNumber + 1 > LEADER_SPEED_MAX_UPGRADE_NUMBER) {
		return;
	}
	if (PlayerStatePtr->GetMineralAmount() < UpgradeLeaderSpeedPrice) {
		return;
	}
	UpgradeLeaderSpeedNumber += 1;
	SetText(UpgradeLeaderSpeedText, "Upgrade Leader Speed: ", UpgradeLeaderSpeedPrice,
		UpgradeLeaderSpeedNumber, LEADER_SPEED_MAX_UPGRADE_NUMBER);
	PlayerControllerPtr->UpgradeLeaderSpeed();
	PlayerStatePtr->RemoveMineral(UpgradeLeaderSpeedPrice);
	UpgradeLeaderSpeedPrice += 200;
}

void UResearchCenterMenu::SetText(UTextBlock* Text, FString Base,  const float Price, const float UpgradeLevel, const float MaxUpgradeLevel)
{
	if (Price != 0)
		Base.Append(FString::FromInt(Price));
	Base.Append(" (");
	Base.Append(FString::FromInt(UpgradeLevel));
	Base.Append("/");
	Base.Append(FString::FromInt(MaxUpgradeLevel));
	Base.Append(")");
	const FText NewText = FText::FromString(*Base);
	Text->SetText(NewText);
}
