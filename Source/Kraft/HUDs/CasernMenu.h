// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#define UNIT_PRICE 50

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "Kraft/Player/KraftPlayerController.h"

#include "CasernMenu.generated.h"

/**
 * 
 */
UCLASS()
class KRAFT_API UCasernMenu : public UUserWidget
{
	GENERATED_BODY()

	public:
	virtual void NativeConstruct() override;

	UFUNCTION()
    void OnSpawnUnitButtonClicked();

	protected:
	UPROPERTY()
	AKraftPlayerController* PlayerControllerPtr;
	
	UPROPERTY()
	AKraftPlayerState* PlayerStatePtr;

	UPROPERTY(meta = (BindWidget))
	UButton* SpawnUnitButton;
};
