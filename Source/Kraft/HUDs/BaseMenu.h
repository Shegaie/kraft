// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "Kraft/Player/KraftPlayerController.h"


#include "BaseMenu.generated.h"

#define WORKER_PRICE 50

/**
 * 
 */
UCLASS()
class KRAFT_API UBaseMenu : public UUserWidget
{
	GENERATED_BODY()

public:
	virtual void NativeConstruct() override;

	UFUNCTION()
	void OnSpawnWorkerButtonClicked();

protected:
	UPROPERTY()
	AKraftPlayerController* PlayerControllerPtr;
	
	UPROPERTY()
	AKraftPlayerState* PlayerStatePtr;

	UPROPERTY(meta = (BindWidget))
	UButton* SpawnWorkerButton;
};
