// Fill out your copyright notice in the Description page of Project Settings.


#include "VCSMenu.h"

#include "Blueprint/WidgetTree.h"

#include "Components/Button.h"

void UVCSMenu::NativeConstruct()
{
	Super::NativeConstruct();

	PlayerControllerPtr = GetOwningPlayer<AKraftPlayerController>();
	PlayerStatePtr = PlayerControllerPtr->GetPlayerState<AKraftPlayerState>();


	SpawnBaseButton->OnClicked.AddUniqueDynamic(this, &UVCSMenu::OnSpawnBaseButtonClicked);
	SpawnCasernButton->OnClicked.AddUniqueDynamic(this, &UVCSMenu::OnSpawnCasernButtonClicked);
	SpawnResearchCenterButton->OnClicked.AddUniqueDynamic(this, &UVCSMenu::OnSpawnResearchCenterButtonClicked);
}

void UVCSMenu::OnSpawnBaseButtonClicked()
{
	if (PlayerStatePtr->GetMineralAmount() > BASE_PRICE) {
		PlayerStatePtr->RemoveMineral(BASE_PRICE);
		PlayerControllerPtr->SpawnGhostBase();
	}
}

void UVCSMenu::OnSpawnCasernButtonClicked()
{
	if (PlayerStatePtr->GetMineralAmount() > CASERN_PRICE) {
		PlayerStatePtr->RemoveMineral(CASERN_PRICE);
		PlayerControllerPtr->SpawnGhostCasern();
	}
}

void UVCSMenu::OnSpawnResearchCenterButtonClicked()
{
	if (PlayerStatePtr->GetMineralAmount() > RESEARCH_CENTER_PRICE) {
		PlayerStatePtr->RemoveMineral(RESEARCH_CENTER_PRICE);
		PlayerControllerPtr->SpawnGhostResearchCenter();
	}
}
