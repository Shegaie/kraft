// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#define MAX_UPGRADE_NUMBER 5
#define LEADER_SPEED_MAX_UPGRADE_NUMBER 3

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "Kraft/Player/KraftPlayerController.h"
#include "ResearchCenterMenu.generated.h"

UCLASS()
class KRAFT_API UResearchCenterMenu : public UUserWidget
{
	GENERATED_BODY()
	
public:
	virtual void NativeConstruct() override;

	UFUNCTION()
	void OnUpgradeUnitsAttackButtonClicked();

	UFUNCTION()
	void OnUpgradeUnitsHealthButtonClicked();

	UFUNCTION()
	void OnUpgradeLeaderSpeedButtonClicked();

protected:
	UPROPERTY()
	AKraftPlayerController* PlayerControllerPtr;
	
	UPROPERTY()
	AKraftPlayerState* PlayerStatePtr;

	UFUNCTION()
	void SetText(UTextBlock *Text, FString Base, float Price, float UpgradeLevel, float MaxUpgradeLevel);
	
	// BUTTONS
	UPROPERTY(meta = (BindWidget))
	UButton* UpgradeUnitsAttackButton;
	
	UPROPERTY(meta = (BindWidget))
	UButton* UpgradeUnitsHealthButton;

	UPROPERTY(meta = (BindWidget))
	UButton* UpgradeLeaderSpeedButton;

	// TEXTS
	UPROPERTY(meta = (BindWidget))
	UTextBlock* UpgradeUnitsAttackText;
	
	UPROPERTY(meta = (BindWidget))
	UTextBlock* UpgradeUnitsHealthText;
	
	UPROPERTY(meta = (BindWidget))
	UTextBlock* UpgradeLeaderSpeedText;

	// UPGRADES NUMBERS
	UPROPERTY()
	float UpgradeUnitsAttackNumber = 0;

	UPROPERTY()
	float UpgradeUnitsHealthNumber = 0;

	UPROPERTY()
	float UpgradeLeaderSpeedNumber = 0;

	// UPGRADES PRICES
	UPROPERTY()
	float UpgradeUnitsAttackPrice = 100;

	UPROPERTY()
	float UpgradeUnitsHealthPrice = 100;

	UPROPERTY()
	float UpgradeLeaderSpeedPrice = 200;
};
