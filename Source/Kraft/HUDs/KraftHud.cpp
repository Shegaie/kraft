// Fill out your copyright notice in the Description page of Project Settings.

#include "KraftHud.h"

FVector2D AKraftHud::GetMousePos2d()
{
	float PosX;
	float PosY;

	GetOwningPlayerController()->GetMousePosition(PosX, PosY);
	return FVector2D(PosX, PosY);
}

void AKraftHud::DrawHUD()
{
	Super::DrawHUD();

	if (bStartSelecting) {
		CurrentPoint = GetMousePos2d();
		DrawRect(FLinearColor(0, 0, 1, 0.15), InitialPoint.X, InitialPoint.Y, CurrentPoint.X - InitialPoint.X, CurrentPoint.Y - InitialPoint.Y);
		SearchUnits();
		SearchWorkers();
		SearchLeaders();
		SearchBases();
		SearchCaserns();
		SearchResearchCenters();
	}
}

// Search Units
void AKraftHud::SearchUnits()
{
	SelectAllUnits(false);
	FoundUnits.Empty();
	GetActorsInSelectionRectangle<AKraftUnit>(InitialPoint, CurrentPoint, FoundUnits, false, false);
	FoundUnits.RemoveAll([this](AKraftUnit* Val)
    {
		return Val->Color != GetOwningPlayerController()->GetPlayerState<AKraftPlayerState>()->GetColor();
    });
	SelectAllUnits(true);
}

void AKraftHud::SearchWorkers()
{
	SelectAllWorkers(false);
	FoundWorkers.Empty();
	GetActorsInSelectionRectangle<AWorker>(InitialPoint, CurrentPoint, FoundWorkers, false, false);
	FoundWorkers.RemoveAll([this](AWorker* Val)
    {
        return Val->Color != GetOwningPlayerController()->GetPlayerState<AKraftPlayerState>()->GetColor();
    });
	SelectAllWorkers(true);
}

void AKraftHud::SearchLeaders()
{
	SelectAllLeaders(false);
	FoundLeaders.Empty();
	GetActorsInSelectionRectangle<ALeader>(InitialPoint, CurrentPoint, FoundLeaders, false, false);
	FoundLeaders.RemoveAll([this](ALeader* Val)
    {
        return Val->Color != GetOwningPlayerController()->GetPlayerState<AKraftPlayerState>()->GetColor();
    });
	SelectAllLeaders(true);
}

// Select Units
void AKraftHud::SelectAllUnits(const bool NewSelection)
{
	for (AKraftUnit *Units: FoundUnits) {
		if (IsValid(Units))
			Units->Select(NewSelection);
	}
}

void AKraftHud::SelectAllWorkers(const bool NewSelection)
{
	for (AWorker *Units: FoundWorkers) {
		if (IsValid(Units))
			Units->Select(NewSelection);
	}
}

void AKraftHud::SelectAllLeaders(const bool NewSelection)
{
	for (ALeader *Units: FoundLeaders) {
		if (IsValid(Units))
			Units->Select(NewSelection);
	}
}

// Search Buildings
void AKraftHud::SearchCaserns()
{
	SelectAllCaserns(false);
	FoundCaserns.Empty();
	GetActorsInSelectionRectangle<ACasern>(InitialPoint, CurrentPoint, FoundCaserns, false, false);
	FoundCaserns.RemoveAll([this](ACasern* Val)
    {
        return Val->Color != GetOwningPlayerController()->GetPlayerState<AKraftPlayerState>()->GetColor();
    });
	SelectAllCaserns(true);
}

void AKraftHud::SearchBases()
{
	SelectAllBases(false);
	FoundBases.Empty();
	GetActorsInSelectionRectangle<ABase>(InitialPoint, CurrentPoint, FoundBases, false, false);
	FoundBases.RemoveAll([this](ABase* Val)
    {
        return Val->Color != GetOwningPlayerController()->GetPlayerState<AKraftPlayerState>()->GetColor();
    });
	SelectAllBases(true);
}

void AKraftHud::SearchResearchCenters()
{
	SelectAllResearchCenters(false);
	FoundResearchCenters.Empty();
	GetActorsInSelectionRectangle<AResearchCenter>(InitialPoint, CurrentPoint, FoundResearchCenters, false, false);
	FoundResearchCenters.RemoveAll([this](AResearchCenter* Val)
    {
        return Val->Color != GetOwningPlayerController()->GetPlayerState<AKraftPlayerState>()->GetColor();
    });
	SelectAllResearchCenters(true);
}

// Select Buildings
void AKraftHud::SelectAllCaserns(const bool NewSelection)
{
	for (ACasern* Units: FoundCaserns) {
		if (IsValid(Units))
			Units->Select(NewSelection);
	}
}

void AKraftHud::SelectAllBases(const bool NewSelection)
{
	for (ABase* Units: FoundBases) {
		if (IsValid(Units))
			Units->Select(NewSelection);
	}
}

void AKraftHud::SelectAllResearchCenters(const bool NewSelection)
{
	for (AResearchCenter* Units: FoundResearchCenters) {
		if (IsValid(Units))
			Units->Select(NewSelection);
	}
}