// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseMenu.h"

void UBaseMenu::NativeConstruct()
{
	Super::NativeConstruct();

	PlayerControllerPtr = GetOwningPlayer<AKraftPlayerController>();
    
	SpawnWorkerButton->OnClicked.AddUniqueDynamic(this, &UBaseMenu::OnSpawnWorkerButtonClicked);
	PlayerStatePtr = PlayerControllerPtr->GetPlayerState<AKraftPlayerState>();
}

void UBaseMenu::OnSpawnWorkerButtonClicked()
{
	ABase *Base = PlayerControllerPtr->SelectedBases[0];
	FVector PosToSpawn = Base->GetActorLocation();

	//Offset to spawn in front of base
	PosToSpawn.X -= 200;
	PosToSpawn.Z = 220;

	if(PlayerStatePtr->GetMineralAmount() >= WORKER_PRICE) {
		PlayerControllerPtr->SpawnWorker(PosToSpawn,PlayerControllerPtr);
		PlayerStatePtr->RemoveMineral(WORKER_PRICE);
	}
}
