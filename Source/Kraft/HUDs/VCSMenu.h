// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "Kraft/Player/KraftPlayerController.h"
#include "VCSMenu.generated.h"

#define BASE_PRICE 400
#define CASERN_PRICE 150
#define RESEARCH_CENTER_PRICE 250

UCLASS()
class KRAFT_API UVCSMenu : public UUserWidget
{
	GENERATED_BODY()
public:
	virtual void NativeConstruct() override;

	UFUNCTION()
	void OnSpawnBaseButtonClicked();

	UFUNCTION()
	void OnSpawnCasernButtonClicked();
	
	UFUNCTION()
	void OnSpawnResearchCenterButtonClicked();

protected:
	UPROPERTY()
	AKraftPlayerController* PlayerControllerPtr;
		
	UPROPERTY()
	AKraftPlayerState* PlayerStatePtr;

	UPROPERTY(meta = (BindWidget))
	UButton* SpawnBaseButton;

	UPROPERTY(meta = (BindWidget))
	UButton* SpawnCasernButton;

	UPROPERTY(meta = (BindWidget))
	UButton* SpawnResearchCenterButton;

};
